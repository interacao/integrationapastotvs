﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace IntegrationApasTotvs.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IntegrationController : ControllerBase
    {
        #region End Points
        // GET api/integration
        [HttpGet]
        public ActionResult<string> Get()
        {
            return new string("Integration APAS-Totvs API");
        }
        #endregion
    }
}
