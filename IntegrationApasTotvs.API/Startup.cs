﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntegrationApasTotvs.Data.Application.Queries;
using IntegrationApasTotvs.Data.Application.Services;
using IntegrationApasTotvs.Data.Contexts;
using IntegrationApasTotvs.Data.Repositories;
using IntegrationApasTotvs.Domain.Interfaces.Queries;
using IntegrationApasTotvs.Domain.Interfaces.Repositories;
using IntegrationApasTotvs.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace IntegrationApasTotvs.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //Repositories
            services.AddScoped<IItemEnvioRepository, ItemEnvioRepository>();
            services.AddScoped<IProdutoCongressoRepository, ProdutoCongressoRepository>();
            services.AddScoped<IItemPedidoCongressoRepository, ItemPedidoCongressoRepository>();
            services.AddScoped<IPrecoCongressoRepository, PrecoCongressoRepository>();
            services.AddScoped<IItemPedidoOperacionalRepository,ItemPedidoOperacionalRepository>();
            services.AddScoped<IProdutoOperacionalRepository,ProdutoOperacionalRepository>();
            services.AddScoped<IPrecoOperacionalRepository, PrecoOperacionalRepository>();
            services.AddScoped<IPedidoRepository,PedidoRepository>();
            services.AddScoped<IPagamentoRepository, PagamentoRepository>();           
            services.AddScoped<IEmpresaRepository, EmpresaRepository>();

            //Services
            services.AddScoped<IIntegrationService, IntegrationService>();

            //HostedServices
            services.AddSingleton<IHostedService,HostedIntegrationService>();           

            //Queries
            services.AddScoped<IGetNewAndUpdatedItensCongressoToSendQuery, GetNewAndUpdatedItensCongressoToSendQuery>();
            services.AddScoped<IGetNewAndUpdatedItensOperacionalToSendQuery, GetNewAndUpdatedItensOperacionalToSendQuery>();

            //Database Connection
            services.AddDbContext<APASContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("APAS_NEW")));
        }

        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
