﻿using IntegrationApasTotvs.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegrationApasTotvs.Data.Application.Models
{
    public class TotvsRequest
    {
        public string token;
        public string origem;
        public IEnumerable<ItemEnvio> linhas;

        public TotvsRequest(IEnumerable<ItemEnvio> itens)
        {
            origem = "Interacao";
            linhas = itens;
        }
    }
}
