﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegrationApasTotvs.Data.Application.Models
{
    public class TotvsResponse
    {
        public string TOKEN;
        public string Retorno;
        public string XML;
    }
}
