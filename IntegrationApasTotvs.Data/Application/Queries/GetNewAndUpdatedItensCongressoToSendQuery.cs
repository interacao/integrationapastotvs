﻿using Dapper;
using IntegrationApasTotvs.Data.Contexts;
using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Entities.DTOs;
using IntegrationApasTotvs.Domain.Interfaces.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace IntegrationApasTotvs.Data.Application.Queries
{
    public class GetNewAndUpdatedItensCongressoToSendQuery : IGetNewAndUpdatedItensCongressoToSendQuery
    {
        private APASContext _context;
        private readonly IConfiguration _configuration;
        public GetNewAndUpdatedItensCongressoToSendQuery(APASContext context , IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public IEnumerable<ItemPedidoDTO> Execute(DateTime date)
        {
            var linq = from itempedido in _context.ItemPedidoCongressos
                       join produto in _context.ProdutoCongressos on itempedido.TB_ProdutoCongresso_cdProduto equals produto.cdProduto                      
                       join pagamento in _context.Pagamentos on itempedido.TB_Pedido_cdPedido equals pagamento.cdPedido
                       join pedido in  _context.Pedidos on itempedido.TB_Pedido_cdPedido equals pedido.cdPedido 
                       join cielo in _context.MonitoriaCielos on pedido.cdPedido.ToString() equals cielo.cdPedido
                       join empresa in _context.Empresas on pedido.TB_Empresa_cdEmpresa equals empresa.cdEmpresa           
                       where itempedido.dtInclusao >= date || itempedido.dtAlteracao >= date 
                       select new ItemPedidoDTO
                       {
                           ItemId = itempedido.cdItem,
                           TipoItem = produto.dsTipoProdutoTotvs,
                           PedidoId = itempedido.TB_Pedido_cdPedido,
                           DataVenda = itempedido.dtItem,
                           ValorItem = itempedido.Valor_Item,
                           ValorDescontoAdm = itempedido.Vlr_Desconto_Adm,
                           ValorDescontoRegra = itempedido.Valor_Desconto_Regra,
                           ValorDescontoCodigoPromocao = itempedido.Valor_Desconto_Codigo_Promocao,
                           ValorLiquido = itempedido.Valor_Liquido,
                           dtInclusao = itempedido.dtInclusao,
                           dtAlteracao = itempedido.dtAlteracao,
                           CC = produto.dsCC,
                           CR = produto.dsCR,
                           PR = produto.dsPR,
                           CO = produto.dsCO,
                           TipoPagamento = pagamento.cdFormaPagamento,
                           DescricaoItem = produto.nmProdutoPortugues,
                           EmpresaCompra = empresa.dsRazaoSocial,
                           Adquirente = "CIELO",
                           Bandeira = cielo.dsBrand,
                           QtdParcela = 1,
                           NumeroParcela = 1,
                           Nsu = cielo.dsProofOfSale,
                           Autorizacao = cielo.AuthorizationCode,
                           DtVencimento = pedido.dtVencimento,
                           NumCartao = cielo.dsCardNumber
                       };

            var result = linq.AsNoTracking().ToList();

            return result ?? null;
        }

        public List<ItemPedidoDTO> ExecuteDapper(DateTime date)
        {
            var conn = new SqlConnection(_configuration.GetConnectionString("APAS_NEW"));

            var a = conn.ConnectionTimeout;

            var sql = @"SELECT DISTINCT
                           itempedido.cdItem AS ItemId,
                           produto.dsTipoProdutoTotvs AS TipoItem,
                           itempedido.TB_Pedido_cdPedido AS PedidoId,
                           itempedido.dtItem AS DataVenda,
                           itempedido.Valor_Item AS ValorItem,
                           itempedido.Vlr_Desconto_Adm AS ValorDescontoAdm,
                           itempedido.Valor_Desconto_Regra AS ValorDescontoRegra,
                           itempedido.Valor_Desconto_Codigo_Promocao AS ValorDescontoCodigoPromocao,
                           itempedido.Valor_Liquido AS ValorLiquido,
                           itempedido.Valor_Item AS ValorUnitario,
                           1 as QtdItem,
                           itempedido.dtInclusao AS dtInclusao,
                           itempedido.dtAlteracao AS dtAlteracao,
                           produto.dsCC AS CC,
                           produto.dsCR AS CR,
                           produto.dsPR AS PR,
                           produto.dsCO AS CO,
                           pagamento.cdFormaPagamento AS TipoPagamento,
                           produto.nmProdutoPortugues AS DescricaoItem,
                           empresa.dsRazaoSocial AS EmpresaCompra,
                           'CIELO' AS Adquirente,
                           cielo.dsBrand AS Bandeira,
                           1 AS QtdParcela,
                           1 AS NumeroParcela,
                           cielo.dsProofOfSale AS Nsu,
                           cielo.AuthorizationCode AS Autorizacao,
                           cielo.dsTid AS Tid,
                           pedido.dtVencimento AS DtVencimento,
                           cielo.dsCardNumber AS NumCartao
                        FROM TB_DD_ItensPedidoCongresso itempedido WITH (NOLOCK)
                        INNER JOIN TB_DD_ProdutoCongresso produto WITH (NOLOCK) ON itempedido.TB_ProdutoCongresso_cdProduto = produto.cdProduto   
                        INNER JOIN TB_DD_Pagamento pagamento WITH (NOLOCK) ON itempedido.TB_Pedido_cdPedido = pagamento.cdPedido 
                        INNER JOIN TB_DD_Pedido pedido WITH (NOLOCK) ON itempedido.TB_Pedido_cdPedido = pedido.cdPedido 
                        INNER JOIN TB_DD_Empresa empresa WITH (NOLOCK) ON pedido.TB_Empresa_cdEmpresa = empresa.cdEmpresa  
                        LEFT JOIN TB_DD_MonitoriaCieloAPI cielo WITH (NOLOCK) ON pedido.cdPedido = CONVERT(INT, REPLACE(cielo.cdPedido, CHAR(0), ''))         
                        WHERE pedido.TB_Edicao_cdEdicao = 142329
                        AND itempedido.Valor_Liquido != '0,00'
                        AND pagamento.cdFormaPagamento != 7
                        AND (
                           (cielo.cdMonitoriaCieloAPI IS NULL) 
                           OR 
                           (cielo.cdMonitoriaCieloAPI IS NOT NULL AND cielo.dsStatus = 'PaymentConfirmed')
                        )
                        AND (
                           (itempedido.dtInclusao >=  @dateQuery) 
                           OR 
                           (itempedido.dtAlteracao >=  @dateQuery) 
                        )";

            var result = conn.Query<ItemPedidoDTO>(sql, new { dateQuery = date }, commandTimeout: conn.ConnectionTimeout);

            return result.ToList();
        }
    }
}
