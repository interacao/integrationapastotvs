﻿using IntegrationApasTotvs.Data.Application.Services.BaseHostedService;
using IntegrationApasTotvs.Domain.Interfaces.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace IntegrationApasTotvs.Data.Application.Services
{
    public class HostedIntegrationService : HostedService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public HostedIntegrationService(IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            //the service scope repository provide instances of a scoped service.
            //the hosted service needs to be a singleton service.
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                IIntegrationService scopedService = scope.ServiceProvider.GetRequiredService<IIntegrationService>();

                while (!cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        await scopedService.Execute();
                        await Task.Delay(TimeSpan.FromMinutes(60), cancellationToken);
                    }
                    catch (Exception ex)
                    {
                        await Task.Delay(TimeSpan.FromMinutes(60), cancellationToken);
                    }
                }
            }
           
        }
    }
}
