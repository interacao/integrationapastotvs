﻿using Flurl.Http;
using IntegrationApasTotvs.Data.Application.Models;
using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Entities.DTOs;
using IntegrationApasTotvs.Domain.Interfaces.Queries;
using IntegrationApasTotvs.Domain.Interfaces.Repositories;
using IntegrationApasTotvs.Domain.Interfaces.Services;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace IntegrationApasTotvs.Data.Application.Services
{
    public class IntegrationService : IIntegrationService
    {
        private readonly IItemEnvioRepository _itemEnvioRepository;        
        private readonly IGetNewAndUpdatedItensCongressoToSendQuery _getNewAndUpdatedItensCongressoToSendQuery;
        private readonly IGetNewAndUpdatedItensOperacionalToSendQuery _getNewAndUpdatedItensOperacioalToSendQuery;
        private readonly IConfiguration _configuration;

        private List<ItemEnvio> ItemEnvioQueue = new List<ItemEnvio>();
        private DateTime UltimaConsulta= DateTime.ParseExact("04/23/2019", "MM/dd/yyyy", CultureInfo.InvariantCulture);
        private DateTime ConsultaAtual;


        public IntegrationService(IItemEnvioRepository itemEnvioRepository,
                                  IGetNewAndUpdatedItensCongressoToSendQuery getNewAndUpdatedItensCongressoToSendQuery,
                                  IGetNewAndUpdatedItensOperacionalToSendQuery getNewAndUpdatedItensOperacioalToSendQuery,
                                  IConfiguration configuration)
        {
            _itemEnvioRepository = itemEnvioRepository;
            _getNewAndUpdatedItensCongressoToSendQuery = getNewAndUpdatedItensCongressoToSendQuery;
            _getNewAndUpdatedItensOperacioalToSendQuery = getNewAndUpdatedItensOperacioalToSendQuery;
            _configuration = configuration;
        }

        #region Public Methods
        public async Task Execute()
        {
            ItemEnvioQueue.Clear();
            ItemEnvioQueue.AddRange(GetItemsWithError());

            if (ItemEnvioQueue.Count() > 0)
            {
                UpdateRequestingDates();
                await SendToTotvsERPAsync(ItemEnvioQueue);
                ItemEnvioQueue.Clear();
            }

            ConsultaAtual = DateTime.Now;

            ItemEnvioQueue.AddRange(GetItensNewAndUpdatedCongresso(UltimaConsulta));
            ItemEnvioQueue.AddRange(GetItensNewAndUpdatedOperacional(UltimaConsulta));

            if (ItemEnvioQueue.Count() > 0)
            {
                SaveNewItemsDapper();
                var listsToSend = splitList(ItemEnvioQueue);
                foreach (List<ItemEnvio> list in listsToSend)
                {                   
                    UpdateRequestingDates();
                    await SendToTotvsERPAsync(list);
                }
            }
            
            UltimaConsulta = ConsultaAtual;
        }
        #endregion

        #region Internal Methods
        private List<ItemEnvio> GetItemsWithError()
        {
            return _itemEnvioRepository.GetItemsWithError(GetSetting<int>("maxTentativasEnvio")).ToList();
        }

        private void SaveNewItems(List<ItemEnvio> itensEnvio)
        {
            _itemEnvioRepository.AddRange(itensEnvio);
            _itemEnvioRepository.SaveChanges();               
        }

        private void SaveNewItemsDapper()
        {
            int id = 0;
            foreach(ItemEnvio item in ItemEnvioQueue)
            {
                string a = JsonConvert.SerializeObject(item);
                id = _itemEnvioRepository.InsertDapperSingle(item);
                item.item_envio_id = id;
            }
        }

        private List<ItemEnvio> GetItensNewAndUpdatedCongresso(DateTime date)
        {
            var ItemCongressoList = _getNewAndUpdatedItensCongressoToSendQuery.ExecuteDapper(date);
            return ConvertoItemCongressoDTOToItemPedido(ItemCongressoList, "Convite");
        }

        private List<ItemEnvio> GetItensNewAndUpdatedOperacional(DateTime date)
        {
            var ItemOperacionalList = _getNewAndUpdatedItensOperacioalToSendQuery.ExecuteDapper(date);
            return ConvertoItemCongressoDTOToItemPedido(ItemOperacionalList, "Serviço");
        }

        private async Task SendToTotvsERPAsync(List<ItemEnvio> itensEnvio)
        {
            TotvsRequest totvsRequest = AssembleTotvsRequest(itensEnvio);

            string url = GetSetting<string>("urlTotvs");

            try
            {
                TotvsResponse response = await url.PostJsonAsync(totvsRequest).ReceiveJson<TotvsResponse>();

                SaveResponsesFromTotvsERP(response, itensEnvio);
            }
            catch (Exception e)
            {
                SaveResponsesFromTotvsERP(new TotvsResponse(), itensEnvio);
            }
        }

        private void SaveResponsesFromTotvsERP(TotvsResponse responseMessage, List<ItemEnvio> itemEnvios)
        {
            if (String.IsNullOrEmpty(responseMessage.XML))
            {
                foreach (ItemEnvio item in itemEnvios)
                {                   
                    item.status_envio = (int)ItemEnvioStatus.Error;
                    item.mensagem_retorno = "Houve um erro de comunicação com o serviço de integração.";
                }
            }
            else
            {
                var reponseXML = new XmlDocument();
                reponseXML.LoadXml(responseMessage.XML);

                foreach (ItemEnvio item in itemEnvios)
                {
                    item.tentativas_envio++;

                    string status = reponseXML.SelectSingleNode("APAS_INTFEIRA/IDENT_BANCARIO_" + item.ident_bancario + "/STATUS").InnerText;
                    item.status_envio = status == "OK" ? (int)ItemEnvioStatus.OK : (int)ItemEnvioStatus.Error;

                    string reponseMessage = reponseXML.SelectSingleNode("APAS_INTFEIRA/IDENT_BANCARIO_" + item.ident_bancario + "/MENSAGEN").InnerText;
                    item.mensagem_retorno = reponseMessage;

                    if (item.mensagem_retorno.Contains("Identificador nao localizado") || item.mensagem_retorno.Contains("Registro ja existente"))
                    {
                        item.tentativas_envio = GetSetting<int>("maxTentativasEnvio");
                    }                    
                }
            }
          
            _itemEnvioRepository.UpdateDapper(itemEnvios);
        }

        private List<ItemEnvio> ConvertoItemCongressoDTOToItemPedido(IEnumerable<ItemPedidoDTO> itemsPedido, string tipoItem)
        {
            List<ItemEnvio> itensEnvio = new List<ItemEnvio>();

            foreach (ItemPedidoDTO itemPedido in itemsPedido)
            {
                ItemEnvio itemEnvio = new ItemEnvio();

                if(itemPedido.dtAlteracao == null)
                {
                    itensEnvio.Add(ConvetToItemEnvioSingle(itemPedido, "I", tipoItem));
                }
                else
                {
                    itensEnvio.Add(ConvetToItemEnvioSingle(itemPedido, "E", tipoItem));
                    itensEnvio.Add(ConvetToItemEnvioSingle(itemPedido, "I", tipoItem));
                }
            }
            return itensEnvio;
        }

        private ItemEnvio ConvetToItemEnvioSingle(ItemPedidoDTO itemPedido, string action, string tipoItem)
        {
            ItemEnvio itemEnvio = new ItemEnvio();
            var culture = new CultureInfo("pt-BR");

            itemEnvio.acao = action;
            itemEnvio.data_venda = itemPedido.DataVenda.Value.ToString("yyyyMMdd");
            itemEnvio.tipo = itemPedido.TipoItem == null || itemPedido.TipoItem == ""? tipoItem : itemPedido.TipoItem;
            itemEnvio.descricao_tipo = itemPedido.DescricaoItem + " - " + itemPedido.EmpresaCompra;
            itemEnvio.cc = itemPedido.CC;
            itemEnvio.cr = itemPedido.CR;
            itemEnvio.pr = itemPedido.PR;
            itemEnvio.co = itemPedido.CO;
            itemEnvio.quantidade = itemPedido.QtdItem.Contains(",") ? decimal.Parse(itemPedido.QtdItem, culture) : decimal.Parse(itemPedido.QtdItem.Replace(".", ","), culture);
            itemEnvio.valor_liquido = decimal.Parse(itemPedido.ValorLiquido, culture);
            itemEnvio.valor_bruto = decimal.Parse(itemPedido.ValorItem, culture);
            itemEnvio.valor_desconto = 0;
            itemEnvio.valor_desconto += itemPedido.ValorDescontoAdm == null || itemPedido.ValorDescontoAdm == "" ? 0 : decimal.Parse(itemPedido.ValorDescontoAdm, culture);
            itemEnvio.valor_desconto += itemPedido.ValorDescontoRegra == null || itemPedido.ValorDescontoRegra == "" ? 0 : decimal.Parse(itemPedido.ValorDescontoRegra, culture);
            itemEnvio.valor_desconto += itemPedido.ValorDescontoCodigoPromocao == null || itemPedido.ValorDescontoCodigoPromocao == "" ? 0 : decimal.Parse(itemPedido.ValorDescontoCodigoPromocao, culture);
            itemEnvio.valor_acrescimo = 0;
            itemEnvio.valor_unitario = decimal.Parse(itemPedido.ValorUnitario, culture);
            itemEnvio.tipo_pagamento = FormasPagamento[itemPedido.TipoPagamento.Value].ToString();
            itemEnvio.moeda = "1";
            itemEnvio.data_envio = null;
            itemEnvio.status_envio = (int)ItemEnvioStatus.Added;
            itemEnvio.pedido_origem = itemPedido.PedidoId.ToString();
            itemEnvio.item_origem = itemPedido.ItemId.ToString();
            itemEnvio.tentativas_envio = 0;
            itemEnvio.mensagem_retorno = "";
            itemEnvio.ident_bancario = itemEnvio.pedido_origem + itemEnvio.item_origem;
            itemEnvio.adquirente = itemPedido.Bandeira == null || itemPedido.Bandeira == "" ? "BRADESCO":"CIELO";
            itemEnvio.bandeira = itemPedido.Bandeira;
            itemEnvio.qtd_parcela = 1;
            itemEnvio.numero_parcela = 1;
            itemEnvio.nsu = itemPedido.Nsu;
            itemEnvio.autorizacao = itemPedido.Autorizacao;
            itemEnvio.data_vencimento = itemPedido.DtVencimento.Value.ToString("yyyyMMdd");
            itemEnvio.numero_cartao = itemPedido.NumCartao;
            itemEnvio.tid = itemPedido.Tid;

            return itemEnvio;
        }

        private void UpdateRequestingDates()
        {
            var date = DateTime.Now;

            foreach (ItemEnvio item in ItemEnvioQueue)
            {
                item.data_envio = date;
            }
        }

        private TotvsRequest AssembleTotvsRequest(IEnumerable<ItemEnvio> itensEnvio)
        {
            var totvsRequest = new TotvsRequest(itensEnvio);
            totvsRequest.token = GetSetting<string>("tokenTotvs");

            return totvsRequest;
        }

        private List<List<ItemEnvio>> splitList(List<ItemEnvio> itensEnvio)
        {
            var sortedList = new List<List<ItemEnvio>>();
            int nSize = GetSetting<int>("maxItensPerSend");

            var excludeList = itensEnvio.Where(x => x.acao.Equals("E")).ToList();
            var insertList = itensEnvio.Where(x => x.acao.Equals("I")).ToList();

            if (excludeList.Count() > 0)
            {
                for (int i = 0; i < excludeList.Count; i += nSize)
                {
                    sortedList.Add(excludeList.GetRange(i, Math.Min(nSize, excludeList.Count - i)));
                }
            }

            if (insertList.Count() > 0)
            {
                for (int i = 0; i < insertList.Count; i += nSize)
                {
                    sortedList.Add(insertList.GetRange(i, Math.Min(nSize, insertList.Count - i)));
                }
            }

            return sortedList;
        }

        /// <summary>
        /// Get the value of [Settings>settingName] on appsetings.json in the API project and returns it as the type informed in T.
        /// </summary>
        /// <returns>Settings>settingName as T</returns>
        private T GetSetting<T>(string settingName)
        {
            return _configuration.GetValue<T>("Settings:"+ settingName);
        }
        #endregion

        #region Enums and Dictionaries
        Dictionary<int, int> FormasPagamento = new Dictionary<int, int>()
        {
            {1,1},	  //BOLETO
            {2,2},	  //VISA
            {3,4},	  //DINHEIRO
            {4,3},	  //VISA DEBITO
            {5,2},	  //MASTERCARD
            {6,2},	  //AMEX
            {7,6},	  //CORTESIA
            {8,6},	  //OUTRO
            {9,6},	  //INVOICE
            {10,6},	  //CHEQUE
            {11,6},	  //COMPROVANTE
            {12,6},	  //EMPENHO
            {13,6},	  //PROMOCIONAL
            {14,2},	  //CIELO
            {15,5},	  //DEPOSITO
            {16,5},	  //PAGAMENTO A VISTA(TEF OU CDC)
            {17,2},	  //CARTAO DE CREDITO
            {18,6},	  //PAYPAL
            {19,2},	  //ELO
            {20,2},	  //DINERS
            {21,2},	  //DISCOVER
            {22,2},	  //AURA
            {23,2},	  //JCB
            {24,6},	  //CORTESIA
            {26,6},	  //Associado
            {27,4},	  //MOEDA ESTRANGEIRA
            {28,2},	  //SOROCRED
            {29,2},	  //AGIPLAN
            {30,2},	  //BANESCARD
            {31,2},	  //CABAL
            {32,2},	  //CREDSYSTEM
            {33,2},	  //CREDZ
            {35,2},	  //HIPERCARD
            {36,6},	  //ISENTO
            {2034,3}, //MASTERCARD DEBITO
            {2036,2}, //ELO CRÉDITO
            {2037,3}, //ELO DÉBITO
            {2039,2}, //MASTERCARD CRÉDITO
            {2040,2}, //VISA CRÉDITO
            {2041,3}, //TEF DEBITO
            {2042,2}  //TEF CREDITO
        };

        enum ItemEnvioStatus
        {
            Added,            // 0
            Error,            // 1
            OK,               // 2
            MaxErrorsReached  // 3
        }
        #endregion
    }
}
