﻿using IntegrationApasTotvs.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;

namespace IntegrationApasTotvs.Data.Contexts
{
    public class APASContext : DbContext
    {
        public DbSet<ItemEnvio> ItemEnvios { get; set; }
        public DbSet<Pedido> Pedidos { get; set; }
        public DbSet<ProdutoCongresso> ProdutoCongressos { get; set; }
        public DbSet<ItemPedidoCongresso> ItemPedidoCongressos { get; set; }
        public DbSet<PrecoCongresso> PrecoCongressos { get; set; }
        public DbSet<ProdutoOperacional> ProdutoOperacionals { get; set; }
        public DbSet<ItemPedidoOperacional> ItemPedidoOperacionals { get; set; }
        public DbSet<PrecoOperacional> PrecoOperacionals { get; set; }
        public DbSet<Pagamento> Pagamentos { get; set; }       
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<MonitoriaCielo> MonitoriaCielos { get; set; }

        public APASContext(DbContextOptions<APASContext> options)
            : base(options)
        {
            this.Database.SetCommandTimeout(200);
        }

        public override int SaveChanges()
        {
            var entities = from e in ChangeTracker.Entries()
                           where //e.State == EntityState.Added || 
                           e.State == EntityState.Modified
                           select e.Entity;
            foreach (var entity in entities)
            {
                var validationContext = new ValidationContext(entity);
                Validator.ValidateObject(entity, validationContext);
            }

            return base.SaveChanges();
        }
    }
}
