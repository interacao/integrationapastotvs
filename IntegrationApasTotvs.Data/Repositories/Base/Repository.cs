﻿using IntegrationApasTotvs.Data.Contexts;
using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;

namespace IntegrationApasTotvs.Data.Repositories.Base
{
    public class Repository<TEntity> : IDisposable, IRepository<TEntity> where TEntity : class
    {
        protected readonly APASContext _context;

        public Repository(APASContext context)
        {
            _context = context;
        }

        public virtual void Add(TEntity obj)
        {
            _context.Set<TEntity>().Add(obj);
        }

        public virtual void AddRange(IEnumerable<TEntity> objs)
        {
            _context.Set<TEntity>().AddRange(objs);
        }

        public virtual void UpdateRange(IEnumerable<TEntity> objs)
        {
            _context.Set<TEntity>().UpdateRange(objs);
        }

        public void Update(TEntity obj)
        {
            _context.Set<TEntity>().Update(obj);
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }

        public TEntity GetById(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public void Remove(int id)
        {
            _context.Set<TEntity>().Remove(_context.Set<TEntity>().Find(id));
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _context.Set<TEntity>();
        }

        public void DetachAll()
        {

            //_context.Entry(entity).State = EntityState.Detached;


            foreach (EntityEntry dbEntityEntry in this._context.ChangeTracker.Entries<ItemEnvio>())
            {

                if (dbEntityEntry.Entity != null)
                {
                    dbEntityEntry.State = EntityState.Detached;
                }
            }
        }
    }
}
