﻿using IntegrationApasTotvs.Data.Contexts;
using IntegrationApasTotvs.Data.Repositories.Base;
using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories;

namespace IntegrationApasTotvs.Data.Repositories
{
    public class EmpresaRepository : Repository<Empresa>, IEmpresaRepository
    {
        public EmpresaRepository(APASContext context)
           : base(context)
        {
        }
    }
}
