﻿using Dapper;
using IntegrationApasTotvs.Data.Contexts;
using IntegrationApasTotvs.Data.Repositories.Base;
using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace IntegrationApasTotvs.Data.Repositories
{
    public class ItemEnvioRepository : Repository<ItemEnvio>, IItemEnvioRepository
    {
        private readonly IConfiguration _configuration;
        public ItemEnvioRepository(APASContext context, IConfiguration configuration)
           : base(context)
        {
            _configuration = configuration;
        }

        public IEnumerable<ItemEnvio> GetItemsWithError(int tentativas)
        {
            return _context.ItemEnvios.Where(x => x.tentativas_envio < tentativas && x.status_envio == 1);
        }

        public IEnumerable<ItemEnvio> GetUnsentItems()
        {
            return _context.ItemEnvios.Where(x => x.status_envio == 0);
        }

        public void UpdateDapper(List<ItemEnvio> itensenvio)
        {
            var conn = new SqlConnection(_configuration.GetConnectionString("APAS_NEW"));
            var sql = "";
            foreach (ItemEnvio item in itensenvio)
            {
                sql = @"UPDATE ItemEnvio 
                        SET
                            acao = @acao,
                            data_venda = @data_venda,
                            ident_bancario = @ident_bancario,
                            tipo = @tipo,
                            descricao_tipo = @descricao_tipo,
                            cc = @cc,
                            cr = @cr,
                            pr = @pr,
                            co = @co,
                            quantidade = @quantidade,
                            valor_liquido = @valor_liquido,
                            valor_bruto = @valor_bruto,
                            valor_desconto = @valor_desconto,
                            valor_acrescimo = @valor_acrescimo,
                            valor_unitario = @valor_unitario,
                            tipo_pagamento = @tipo_pagamento,
                            moeda = @moeda,
                            data_envio = @data_envio,
                            status_envio = @status_envio,
                            pedido_origem = @pedido_origem,
                            item_origem = @item_origem,
                            tentativas_envio = @tentativas_envio,
                            mensagem_retorno = @mensagem_retorno,
                            adquirente = @adquirente,
                            bandeira = @bandeira,
                            qtd_parcela = @qtd_parcela,
                            numero_parcela = @numero_parcela,
                            nsu = @nsu,
                            autorizacao = @autorizacao,
                            data_vencimento = @data_vencimento,
                            numero_cartao = @numero_cartao
                        WHERE
                            item_envio_id = @item_envio_id";

                var result = conn.Execute(sql, new {
                    item_envio_id = item.item_envio_id,
                    acao = item.acao,
                    data_venda = item.data_venda,
                    ident_bancario = item.ident_bancario,
                    tipo = item.tipo,
                    descricao_tipo = item.descricao_tipo,
                    cc = item.cc,
                    cr = item.cr,
                    pr = item.pr,
                    co = item.co,
                    quantidade = item.quantidade,
                    valor_liquido = item.valor_liquido,
                    valor_bruto = item.valor_bruto,
                    valor_desconto = item.valor_desconto,
                    valor_acrescimo = item.valor_acrescimo,
                    valor_unitario = item.valor_unitario,
                    tipo_pagamento = item.tipo_pagamento,
                    moeda = item.moeda,
                    data_envio = item.data_envio,
                    status_envio = item.status_envio,
                    pedido_origem = item.pedido_origem,
                    item_origem = item.item_origem,
                    tentativas_envio = item.tentativas_envio,
                    mensagem_retorno = item.mensagem_retorno,
                    adquirente = item.adquirente,
                    bandeira = item.bandeira,
                    qtd_parcela = item.qtd_parcela,
                    numero_parcela = item.numero_parcela,
                    nsu = item.nsu,
                    autorizacao = item.autorizacao,
                    data_vencimento = item.data_vencimento,
                    numero_cartao = item.numero_cartao
                });
            }
        }

        public void InsertDapper(List<ItemEnvio> itensenvio)
        {
            var conn = new SqlConnection(_configuration.GetConnectionString("APAS_NEW"));
            var sql = "";
            foreach (ItemEnvio item in itensenvio)
            {
                sql = @"INSERT INTO ItemEnvio
                        OUTPUT INSERTED.item_envio_id
                        VALUES(
                            acao = @acao,
                            data_venda = @data_venda,
                            ident_bancario = @ident_bancario,
                            tipo = @tipo,
                            descricao_tipo = @descricao_tipo,
                            cc = @cc,
                            cr = @cr,
                            pr = @pr,
                            co = @co,
                            quantidade = @quantidade,
                            valor_liquido = @valor_liquido,
                            valor_bruto = @valor_bruto,
                            valor_desconto = @valor_desconto,
                            valor_acrescimo = @valor_acrescimo,
                            valor_unitario = @valor_unitario,
                            tipo_pagamento = @tipo_pagamento,
                            moeda = @moeda,
                            data_envio = @data_envio,
                            status_envio = @status_envio,
                            pedido_origem = @pedido_origem,
                            item_origem = @item_origem,
                            tentativas_envio = @tentativas_envio,
                            mensagem_retorno = @mensagem_retorno,
                            adquirente = @adquirente,
                            bandeira = @bandeira,
                            qtd_parcela = @qtd_parcela,
                            numero_parcela = @numero_parcela,
                            nsu = @nsu,
                            autorizacao = @autorizacao,
                            data_vencimento = @data_vencimento,
                            numero_cartao = @numero_cartao
                        );";

                var result = conn.QuerySingle<Guid>(sql, new
                {
                    acao = item.acao,
                    data_venda = item.data_venda,
                    ident_bancario = item.ident_bancario,
                    tipo = item.tipo,
                    descricao_tipo = item.descricao_tipo,
                    cc = item.cc,
                    cr = item.cr,
                    pr = item.pr,
                    co = item.co,
                    quantidade = item.quantidade,
                    valor_liquido = item.valor_liquido,
                    valor_bruto = item.valor_bruto,
                    valor_desconto = item.valor_desconto,
                    valor_acrescimo = item.valor_acrescimo,
                    valor_unitario = item.valor_unitario,
                    tipo_pagamento = item.tipo_pagamento,
                    moeda = item.moeda,
                    data_envio = item.data_envio,
                    status_envio = item.status_envio,
                    pedido_origem = item.pedido_origem,
                    item_origem = item.item_origem,
                    tentativas_envio = item.tentativas_envio,
                    mensagem_retorno = item.mensagem_retorno,
                    adquirente = item.adquirente,
                    bandeira = item.bandeira,
                    qtd_parcela = item.qtd_parcela,
                    numero_parcela = item.numero_parcela,
                    nsu = item.nsu,
                    autorizacao = item.autorizacao,
                    data_vencimento = item.data_vencimento,
                    numero_cartao = item.numero_cartao
                });
            }
        }

        public int InsertDapperSingle(ItemEnvio itemenvio)
        {
            var conn = new SqlConnection(_configuration.GetConnectionString("APAS_NEW"));
            var sql = "";
            
            sql = @"INSERT INTO ItemEnvio
                    OUTPUT INSERTED.item_envio_id
                    VALUES(
                        @acao,
                        @data_venda,
                        @ident_bancario,
                        @tipo,
                        @descricao_tipo,
                        @cc,
                        @cr,
                        @pr,
                        @co,
                        @quantidade,
                        @valor_liquido,
                        @valor_bruto,
                        @valor_desconto,
                        @valor_acrescimo,
                        @valor_unitario,
                        @tipo_pagamento,
                        @moeda,
                        @data_envio,
                        @status_envio,
                        @pedido_origem,
                        @item_origem,
                        @tentativas_envio,
                        @mensagem_retorno,
                        @adquirente,
                        @bandeira,
                        @qtd_parcela,
                        @numero_parcela,
                        @nsu,
                        @autorizacao,
                        @data_vencimento,
                        @numero_cartao,
                        @tid
                    );";

            var result = conn.QuerySingle<int>(sql, new
            {
                acao = itemenvio.acao,
                data_venda = itemenvio.data_venda,
                ident_bancario = itemenvio.ident_bancario,
                tipo = itemenvio.tipo,
                descricao_tipo = itemenvio.descricao_tipo,
                cc = itemenvio.cc,
                cr = itemenvio.cr,
                pr = itemenvio.pr,
                co = itemenvio.co,
                quantidade = itemenvio.quantidade,
                valor_liquido = itemenvio.valor_liquido,
                valor_bruto = itemenvio.valor_bruto,
                valor_desconto = itemenvio.valor_desconto,
                valor_acrescimo = itemenvio.valor_acrescimo,
                valor_unitario = itemenvio.valor_unitario,
                tipo_pagamento = itemenvio.tipo_pagamento,
                moeda = itemenvio.moeda,
                data_envio = itemenvio.data_envio,
                status_envio = itemenvio.status_envio,
                pedido_origem = itemenvio.pedido_origem,
                item_origem = itemenvio.item_origem,
                tentativas_envio = itemenvio.tentativas_envio,
                mensagem_retorno = itemenvio.mensagem_retorno,
                adquirente = itemenvio.adquirente,
                bandeira = itemenvio.bandeira,
                qtd_parcela = itemenvio.qtd_parcela,
                numero_parcela = itemenvio.numero_parcela,
                nsu = itemenvio.nsu,
                autorizacao = itemenvio.autorizacao,
                data_vencimento = itemenvio.data_vencimento,
                numero_cartao = itemenvio.numero_cartao,
                tid = itemenvio.tid
            });
            return result;
        }
    }
}
