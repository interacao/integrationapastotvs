﻿using System;
using System.Collections.Generic;
using System.Linq;
using IntegrationApasTotvs.Data.Contexts;
using IntegrationApasTotvs.Data.Repositories.Base;
using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace IntegrationApasTotvs.Data.Repositories
{
    public class ItemPedidoCongressoRepository : Repository<ItemPedidoCongresso>, IItemPedidoCongressoRepository
    {
        public ItemPedidoCongressoRepository(APASContext context)
           : base(context)
        {
        }

        public IEnumerable<ItemPedidoCongresso> GetNewAndUpdatedNoTracking(DateTime date)
        {
            return _context.ItemPedidoCongressos.Where(x => x.dtInclusao >= date || x.dtAlteracao >= date).AsNoTracking();
        }
    }
}
