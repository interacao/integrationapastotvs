﻿using IntegrationApasTotvs.Data.Contexts;
using IntegrationApasTotvs.Data.Repositories.Base;
using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IntegrationApasTotvs.Data.Repositories
{
    public class ItemPedidoOperacionalRepository : Repository<ItemPedidoOperacional>, IItemPedidoOperacionalRepository
    {
        public ItemPedidoOperacionalRepository(APASContext context)
           : base(context)
        {
        }

        public IEnumerable<ItemPedidoOperacional> GetNewAndUpdatedNoTracking(DateTime date)
        {
            return _context.ItemPedidoOperacionals.Where(x => x.dtInclusao >= date || x.dtAlteracao >= date).AsNoTracking();
        }
    }
}
