﻿using System.Collections.Generic;
using IntegrationApasTotvs.Data.Contexts;
using IntegrationApasTotvs.Data.Repositories.Base;
using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace IntegrationApasTotvs.Data.Repositories
{
    public class PagamentoRepository : Repository<Pagamento>, IPagamentoRepository
    {
        public PagamentoRepository(APASContext context)
           : base(context)
        {
        }

        public IEnumerable<Pagamento> GetByPedidoNoTracking(int pedidoId)
        {
            return _context.Pagamentos.Where(x => x.cdPedido == pedidoId).AsNoTracking();
        }
    }
}
