﻿using IntegrationApasTotvs.Data.Contexts;
using IntegrationApasTotvs.Data.Repositories.Base;
using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories;

namespace IntegrationApasTotvs.Data.Repositories
{
    public class PrecoOperacionalRepository : Repository<PrecoOperacional>, IPrecoOperacionalRepository
    {
        public PrecoOperacionalRepository(APASContext context)
          : base(context)
        {
        }
    }
}
