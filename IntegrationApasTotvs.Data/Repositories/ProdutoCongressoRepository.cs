﻿using IntegrationApasTotvs.Data.Contexts;
using IntegrationApasTotvs.Data.Repositories.Base;
using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories;

namespace IntegrationApasTotvs.Data.Repositories
{
    public class ProdutoCongressoRepository : Repository<ProdutoCongresso>, IProdutoCongressoRepository
    {
        public ProdutoCongressoRepository(APASContext context)
          : base(context)
        {
        }
    }
}
