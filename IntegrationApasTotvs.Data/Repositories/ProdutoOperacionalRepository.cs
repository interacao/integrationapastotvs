﻿using IntegrationApasTotvs.Data.Contexts;
using IntegrationApasTotvs.Data.Repositories.Base;
using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories;

namespace IntegrationApasTotvs.Data.Repositories
{
    public class ProdutoOperacionalRepository : Repository<ProdutoOperacional>, IProdutoOperacionalRepository
    {
        public ProdutoOperacionalRepository(APASContext context)
          : base(context)
        {
        }
    }
}
