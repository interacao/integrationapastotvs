﻿using System;

namespace IntegrationApasTotvs.Domain.Entities.DTOs
{
    public class ItemPedidoDTO
    {
        public int ItemId { get; set; }
        public string TipoItem { get; set; }
        public int PedidoId { get; set; }
        public DateTime? DataVenda { get; set; }
        public string ValorItem { get; set; }
        public string ValorDescontoAdm { get; set; }
        public string ValorDescontoRegra { get; set; }
        public string ValorDescontoCodigoPromocao { get; set; }
        public string ValorLiquido { get; set; }
        public string ValorUnitario { get; set; }
        public string QtdItem { get; set; }
        public DateTime? dtInclusao { get; set; }
        public DateTime? dtAlteracao { get; set; }
        public string CC { get; set; }
        public string CR { get; set; }
        public string PR { get; set; }
        public string CO { get; set; }
        public int? TipoPagamento { get; set; }
        public string DescricaoItem { get; set; }
        public string EmpresaCompra { get; set; }
        public string Adquirente { get; set; }
        public string Bandeira { get; set; }
        public int? QtdParcela { get; set; }
        public int? NumeroParcela { get; set; }
        public string Nsu { get; set; }
        public string Autorizacao { get; set; }
        public string Tid { get; set; }
        public DateTime? DtVencimento { get; set; }
        public string NumCartao { get; set; }
    }
}
