﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace IntegrationApasTotvs.Domain.Entities
{
    [Table("TB_DD_Empresa")]
    public class Empresa
    {
        [Key]
        public int cdEmpresa { get; set; }
        public int TB_TipoEmpresa_cdTipo { get; set; }
        public int? cdTipoDocumento { get; set; }
        public string dsCnpj { get; set; }
        public string dsInscricaoEstadual { get; set; }
        public string dsInscricaoMunicipal { get; set; }
        public string dsRazaoSocial { get; set; }
        public string dsNomeFantasia { get; set; }
        public string dsEmpresaCracha { get; set; }
        public string dsLogradouroComercial { get; set; }
        public string dsComplementoLogradouroComercial { get; set; }
        public string dsNumeroLogradouroComercial { get; set; }
        public string dsCepLogradouroComercial { get; set; }
        public string dsBairroLogradouroComercial { get; set; }
        public string dsCidadeLogradouroComercial { get; set; }
        public string dsUfLogradouroComercial { get; set; }
        public int TB_Pais_cdPaisComercial { get; set; }
        public string dsSite { get; set; }
        public string dsDDITelefoneEmpresa1 { get; set; }
        public string dsDDDTelefoneEmpresa1 { get; set; }
        public string dsNumeroTelefoneEmpresa1 { get; set; }
        public string dsRamalTelefoneEmpresa1 { get; set; }
        public string dsDDITelefoneEmpresa2 { get; set; }
        public string dsDDDTelefoneEmpresa2 { get; set; }
        public string dsNumeroTelefoneEmpresa2 { get; set; }
        public string dsRamalTelefoneEmpresa2 { get; set; }
        public string dsDDIFaxEmpresa { get; set; }
        public string dsDDDFaxEmpresa { get; set; }
        public string dsNumeroFaxEmpresa { get; set; }
        public string dsRamoAtividade { get; set; }
        public string dsAreaAtuacao { get; set; }
        public string dsFaturamento { get; set; }
        public string EnEmail { get; set; }
        public string SINDIPROM { get; set; }
        public string NRegistroPoliciaFederal { get; set; }
        public string dsVeiculo { get; set; }
        public string dsEditoria { get; set; }
        public string dsTwitter { get; set; }
        public string dsFacebook { get; set; }
        public string dsObservacao { get; set; }
        public string dsIdioma { get; set; }
        public string dsResPessoa1 { get; set; }
        public string dsResPessoa2 { get; set; }
        public string dsResPessoa3 { get; set; }
        public string dsResPessoa4 { get; set; }
        public string dsResPessoa5 { get; set; }
        public string dsResPessoa6 { get; set; }
        public string dsResPessoa7 { get; set; }
        public string dsResPessoa8 { get; set; }
        public string dsResPessoa9 { get; set; }
        public string dsResPessoa10 { get; set; }
        public int? cdCnaeSebrae { get; set; }
        public string dsCEPEmpresaSebrae { get; set; }
        public string dsLogradouroEmpresaSebrae { get; set; }
        public string dsComplementoEmpresaSebrae { get; set; }
        public int? cdPaisEmpresaSebrae { get; set; }
        public int? cdEstadoEmpresaSebrae { get; set; }
        public int? cdCidadeEmpresaSebrae { get; set; }
        public int? cdBairroEmpresaSebrae { get; set; }
        public DateTime dtAberturaSebrae { get; set; }
        public string CodConstSebrae { get; set; }
        public string CodProdutoRuralSebrae { get; set; }
        public string CodDapSebrae { get; set; }
        public string CodPescadorSebrae { get; set; }
        public string dsNumeroLogradouroEmpresaSebrae { get; set; }
        public string dsRegimeTributacao { get; set; }
        public int? cdEmpresaOriginal { get; set; }
        public string dsResEmpresa01 { get; set; }
        public string dsSenha { get; set; }
        public string nmResponsavel { get; set; }
        public string dsEmailResponsavel { get; set; }
        public string ACCOUNT_ID_CRM { get; set; }
        public string CodigoClienteAPAS { get; set; }
        public string dsRegionalDistrital { get; set; }
        public int? cdCoIrmas { get; set; }
        public string dsPerfil { get; set; }
        public string dsCodigoCNAE { get; set; }
        public string dsResEmpresa1 { get; set; }
        public string dsResEmpresa2 { get; set; }
        public string dsResEmpresa3 { get; set; }
        public string dsResEmpresa4 { get; set; }
        public string dsResEmpresa5 { get; set; }
        public string dsResEmpresa6 { get; set; }
        public string dsResEmpresa7 { get; set; }
        public string dsResEmpresa8 { get; set; }
        public string dsResEmpresa9 { get; set; }
        public string dsResEmpresa10 { get; set; }
    }
}
