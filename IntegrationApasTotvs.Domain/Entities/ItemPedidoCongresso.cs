﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegrationApasTotvs.Domain.Entities
{
    [Table("TB_DD_ItensPedidoCongresso")]
    public class ItemPedidoCongresso
    {
        [Key]
        public int cdItem { get; set; }
        public int TB_Pessoa_cdPessoa { get; set; }
        public int TB_ProdutoCongresso_cdProduto { get; set; }
        public int TB_Pedido_cdPedido { get; set; }
        public int? TB_Edicao_cdEdicao { get; set; }
        public int? cdPreco { get; set; }
        public DateTime? dtItem { get; set; }
        public string dsSituacao { get; set; }
        public string dsCodigoBarras { get; set; }
        public string dsNome { get; set; }
        public string dsEmail { get; set; }
        public int? cdTicket { get; set; }
        public int? cdItemTroca { get; set; }
        public int? cdPessoalSubstituida { get; set; }
        public DateTime? dtsubstituicao { get; set; }
        public int? cdUsuarioSubstituicao { get; set; }
        public int? cdFicha { get; set; }
        public string dsIngressoEntregue { get; set; }
        public Guid cdPessoaCongresso { get; set; }
        public string Valor_Item { get; set; }
        public string Vlr_Desconto_Adm { get; set; }
        public string Valor_Desconto_Regra { get; set; }
        public string Valor_Desconto_Codigo_Promocao { get; set; }
        public string Valor_Liquido { get; set; }
        public DateTime? dtInclusao { get; set; }
        public int? cdUsuarioInclusao { get; set; }
        public DateTime? dtAlteracao { get; set; }
        public int? cdUsuarioAlteracao { get; set; }
        public int? cdCodigoPromocional { get; set; }
    }
}
