﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegrationApasTotvs.Domain.Entities
{
    [Table("TB_DD_ItensPedidoOperacional")]
    public class ItemPedidoOperacional
    {
        [Key]
        public int cdItem { get; set; }
        public int TB_ProdutoOperacional_cdProduto { get; set; }
        public int TB_Pedido_cdPedido { get; set; }
        public string QtdProduto { get; set; }
        public string dsValor { get; set; }
        public int? cdStand { get; set; }
        public int? cdPessoa { get; set; }
        public string CodigoBarras { get; set; }
        public string dsSituacao { get; set; }
        public string dsDescricaoOpcional { get; set; }
        public int? cdEmpresaSegundario { get; set; }
        public string dsReservado1 { get; set; }
        public string dsReservado2 { get; set; }
        public string dsReservado3 { get; set; }
        public string dsReservado4 { get; set; }
        public string dsReservado5 { get; set; }
        public string dsReservado6 { get; set; }
        public string dsReservado7 { get; set; }
        public string dsReservado8 { get; set; }
        public string dsReservado9 { get; set; }
        public string dsReservado10 { get; set; }
        public string dsReservado11 { get; set; }
        public string dsReservado12 { get; set; }
        public string dsReservado13 { get; set; }
        public string dsReservado14 { get; set; }
        public string dsReservado15 { get; set; }
        public string dsReservado16 { get; set; }
        public string dsReservado17 { get; set; }
        public string dsReservado18 { get; set; }
        public string dsReservado19 { get; set; }
        public string dsReservado20 { get; set; }
        public string Valor_Item { get; set; }
        public string Vlr_Desconto_Adm { get; set; }
        public string Valor_Liquido { get; set; }
        public DateTime? dtInclusao { get; set; }
        public int? cdUsuarioInclusao { get; set; }
        public DateTime? dtAlteracao { get; set; }
        public int? cdUsuarioAlteracao { get; set; }
    }
}
