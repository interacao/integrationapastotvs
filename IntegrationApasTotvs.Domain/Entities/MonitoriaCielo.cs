﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegrationApasTotvs.Domain.Entities
{
    [Table("TB_DD_MonitoriaCieloAPI")]
    public class MonitoriaCielo
    {
        [Key]
        public Guid cdMonitoriaCieloAPI { get; set; }
        public string cdPedido { get; set; }
        public string amount { get; set; }
        public DateTime dtEntrada { get; set; }
        public string dsBrand { get; set; }
        public string dsCardNumber { get; set; }
        public string dsCardToken { get; set; }
        public string dsTitular { get; set; }
        public string dsTid { get; set; }
        public string dsPaymentId { get; set; }
        public string dsProofOfSale { get; set; }
        public DateTime? dtReceivedDate { get; set; }
        public string dsStatus { get; set; }
        public string dsReturnCode { get; set; }
        public string dsReturnMenssage { get; set; }
        public string Type { get; set; }
        public string AuthorizationCode { get; set; }
    }
}
