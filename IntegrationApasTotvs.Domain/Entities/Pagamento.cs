﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegrationApasTotvs.Domain.Entities
{
    [Table("TB_DD_Pagamento")] 
    public class Pagamento
    {
        [Key]
        public int cdPagamento { get; set; }
        public int? cdPedido { get; set; }
        public int? cdFormaPagamento { get; set; }
        public string dsValor { get; set; }
        public string Status { get; set; }
        public DateTime? dtInclusao { get; set; }
        public DateTime? dtCancelamento { get; set; }
        public int? cdUsuarioInclusao { get; set; }
        public int? cdUsuarioCancelamento { get; set; }
    }
}
