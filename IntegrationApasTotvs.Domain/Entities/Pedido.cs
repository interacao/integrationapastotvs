﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegrationApasTotvs.Domain.Entities
{
    [Table("TB_DD_Pedido")]
    public class Pedido
    {
        [Key]
        public int cdPedido { get; set; }
        public int? TB_Edicao_cdEdicao { get; set; }
        public int TB_Empresa_cdEmpresa { get; set; }
        public DateTime? dtPedido { get; set; }
        public DateTime? dtVencimento { get; set; }
        public DateTime? dtPagamento { get; set; }
        public string dsPedido { get; set; }
        public DateTime? dtCancelamento { get; set; }
        public string dsPago { get; set; }
        public string dsSituacao { get; set; }
        public string vlTotal { get; set; }
        public string vlDesconto { get; set; }
        public int? nuParcela { get; set; }
        public string TipoPessoaFaturamento { get; set; }
        public string nmFaturamento { get; set; }
        public string dsCpfCnpjFaturamento { get; set; }
        public string dsEnderecoFaturamento { get; set; }
        public string dsCepfaturamento { get; set; }
        public string dsComplementoFaturamento { get; set; }
        public string dsCidadeFaturamento { get; set; }
        public string dsUfFaturamento { get; set; }
        public string dsInscricaoEstadualFaturamento { get; set; }
        public int? cdPessoa { get; set; }
        public string dsMoeda { get; set; }
        public string dsEstorno { get; set; }
        public string vlAcrescimo { get; set; }
        public int? UsuarioBaixaPagamento { get; set; }
        public int? UsuarioCancelamento { get; set; }
        public string dsObs { get; set; }
        public int? cdTipoPedido { get; set; }
        public string dsIdioma { get; set; }
        public int? cdUsuario { get; set; }
        public string dsNumeroFaturamento { get; set; }
        public string oTid { get; set; }
        public int? cdSistema { get; set; }
        public int? cdResponsavelCongresso { get; set; }
        public int? cdRegraDesconto { get; set; }
        public string dsTid { get; set; }
        public string dsBandeiraCielo { get; set; }
        public int? cdRegraDescontoPorProduto { get; set; }
        public DateTime? dtCancelamentoAutomatico { get; set; }
        public string dsTipoEmpresa { get; set; }
        public string dsDescontoAdministrativo { get; set; }
        public string dsAcrescimoAdministrativo { get; set; }
        public string dsLiberado { get; set; }
        public string dsSaleIdPaypalPlus { get; set; }
        public DateTime? dtSalesPaypalPlus { get; set; }
        public string dsSalesStatusPayPalPlusarchar { get; set; }
        public string dsIncideImposto { get; set; }
        public string vlImposto { get; set; }
        public string vlParaCalculoImpostochar { get; set; }
        public string dsTipoPedido { get; set; }
        public string dsComentarioLiberacaochar { get; set; }
        public string dsObservacaoInternaar { get; set; }
        public string idPagamento { get; set; }
        public string dsOrigemInsercao { get; set; }
    }
}
