﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegrationApasTotvs.Domain.Entities
{
    [Table("TB_DD_PrecoProdutoCongresso")]
    public class PrecoCongresso
    {
        [Key]
        public int cdPreco { get; set; }
        public int TB_ProdutoCongresso_cdProduto { get; set; }
        public DateTime? dtLimite { get; set; }
        public string dsValor { get; set; }
        public string dsValorUS { get; set; }
        public string dsSituacao { get; set; }
        public int? cdSubCategoria { get; set; }
        public string dsCC { get; set; }
        public string dsCR { get; set; }
        public string dsCO { get; set; }
        public string dsPR { get; set; }
        public string dsCodigoNumerico { get; set; }
        public string dsAssociado { get; set; }
    }
}
