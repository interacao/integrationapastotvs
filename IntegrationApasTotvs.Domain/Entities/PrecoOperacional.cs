﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegrationApasTotvs.Domain.Entities
{
    [Table("TB_DD_PrecoProdutoOperacional")]
    public class PrecoOperacional
    {
        [Key]
        public int cdPreco { get; set; }
        public int TB_ProdutoOperacional_cdProduto { get; set; }
        public DateTime? dtLimite { get; set; }
        public string dsValor { get; set; }
        public string dsValorUS { get; set; }
        public string dsSituacao { get; set; }
    }
}
