﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegrationApasTotvs.Domain.Entities
{
    [Table("TB_DD_ProdutoCongresso")]
    public class ProdutoCongresso
    {
        [Key]
        public int cdProduto { get; set; }
        public int? cdEdicao { get; set; }
        public string nmProdutoPortugues { get; set; }
        public string nmProdutoIngles { get; set; }
        public string dsProdutoPortugues { get; set; }
        public string dsProdutoIngles { get; set; }
        public string dsGrupo { get; set; }
        public string dsSituacao { get; set; }
        public string nmProdutoEspanhol { get; set; }
        public string dsProdutoEspanhol { get; set; }
        public int? nuOrdem { get; set; }
        public string TipoProduto { get; set; }
        public string dsSigla { get; set; }
        public string dsDia { get; set; }
        public int? cdUsuarioCadastro { get; set; }
        public DateTime? dtCadastro { get; set; }
        public int? cdUsuarioAlteracao { get; set; }
        public DateTime? dtAlteracao { get; set; }
        public DateTime? dtLimite { get; set; }
        public int? cdSistema { get; set; }
        public int? cdGrupoProdutoCongresso { get; set; }
        public int? nuVagas { get; set; }
        public string dsProdutoCongressoPagote { get; set; }
        public int? nuSubProduto { get; set; }
        public int? nuIdGrupo { get; set; }
        public int? nuQtdColunaSubProduto { get; set; }
        public string dsQtdCargaHoraria { get; set; }
        public DateTime? dtPalestra { get; set; }
        public string dsLocalizacao { get; set; }
        public DateTime? dtPalestraFim { get; set; }
        public int? qtdImpressao { get; set; }
        public string dsSala { get; set; }
        public string dsCompraObrigatoria { get; set; }
        public string dsCC { get; set; }
        public string dsCR { get; set; }
        public string dsCO { get; set; }
        public string dsTipoProdutoTotvs { get; set; }
        public string dsPR { get; set; }
    }
}
