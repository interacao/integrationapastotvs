﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegrationApasTotvs.Domain.Entities
{
    [Table("TB_DD_ProdutoOperacional")]
    public class ProdutoOperacional
    {
        [Key]
        public int cdProduto { get; set; }
        public int TB_Edicao_cdEdicao { get; set; }
        public int nuOrdem { get; set; }
        public string dsProduto { get; set; }
        public string dsProdutoIngles { get; set; }
        public string dsSituacao { get; set; }
        public string SiglaImagem { get; set; }
        public int? cdFormulario { get; set; }
        public int? cdCategoria { get; set; }
        public string dsTipoRegra { get; set; }
        public string dsQuantidade { get; set; }
        public string vlAdicional { get; set; }
        public string dsFecharPedidoAutomatico { get; set; }
        public string dsIncideImposto { get; set; }
        public string dsCC { get; set; }
        public string dsCR { get; set; }
        public string dsCO { get; set; }
        public int? cdFormularioCompraAdicional { get; set; }
        public int? cdProdutoCongresso { get; set; }
        public string dsTipoProdutoTotvs { get; set; }
        public string dsPR { get; set; }
    }
}
