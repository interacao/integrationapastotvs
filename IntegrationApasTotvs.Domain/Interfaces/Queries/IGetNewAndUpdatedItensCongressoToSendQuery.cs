﻿using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegrationApasTotvs.Domain.Interfaces.Queries
{
    public interface IGetNewAndUpdatedItensCongressoToSendQuery
    {
        IEnumerable<ItemPedidoDTO> Execute(DateTime date);
        List<ItemPedidoDTO> ExecuteDapper(DateTime date);
    }
}
