﻿using IntegrationApasTotvs.Domain.Entities.DTOs;
using System;
using System.Collections.Generic;

namespace IntegrationApasTotvs.Domain.Interfaces.Queries
{
    public interface IGetNewAndUpdatedItensOperacionalToSendQuery
    {
        IEnumerable<ItemPedidoDTO> Execute(DateTime date);
        List<ItemPedidoDTO> ExecuteDapper(DateTime date);
    }
}