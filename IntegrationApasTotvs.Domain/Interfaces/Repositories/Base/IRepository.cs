﻿using System.Collections.Generic;

namespace IntegrationApasTotvs.Domain.Interfaces.Repositories.Base
{
    public interface IRepository<TEntity>  where TEntity : class
    {
        void Add(TEntity obj);
        void AddRange(IEnumerable<TEntity> objs);
        TEntity GetById(int id);
        IEnumerable<TEntity> GetAll();
        void Update(TEntity obj);
        void UpdateRange(IEnumerable<TEntity> objs);
        void Remove(int id);
        int SaveChanges();
        void DetachAll();
    }
}
