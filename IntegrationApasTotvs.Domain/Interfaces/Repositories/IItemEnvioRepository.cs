﻿using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;

namespace IntegrationApasTotvs.Domain.Interfaces.Repositories
{
    public interface IItemEnvioRepository : IRepository<ItemEnvio>
    {
        IEnumerable<ItemEnvio> GetItemsWithError(int tentativas);
        IEnumerable<ItemEnvio> GetUnsentItems();
        void UpdateDapper(List<ItemEnvio> itensenvio);
        int InsertDapperSingle(ItemEnvio itemenvio);
    }
}
