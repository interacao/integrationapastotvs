﻿using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;

namespace IntegrationApasTotvs.Domain.Interfaces.Repositories
{
    public interface IItemPedidoCongressoRepository : IRepository<ItemPedidoCongresso>
    {
        IEnumerable<ItemPedidoCongresso> GetNewAndUpdatedNoTracking(DateTime date);
    }
}
