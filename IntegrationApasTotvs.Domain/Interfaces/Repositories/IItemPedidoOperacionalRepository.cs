﻿using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;

namespace IntegrationApasTotvs.Domain.Interfaces.Repositories
{
    public interface IItemPedidoOperacionalRepository : IRepository<ItemPedidoOperacional>
    {
        IEnumerable<ItemPedidoOperacional> GetNewAndUpdatedNoTracking(DateTime date);
    }
}
