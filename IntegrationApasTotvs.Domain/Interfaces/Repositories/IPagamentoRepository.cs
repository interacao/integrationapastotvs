﻿using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories.Base;
using System.Collections.Generic;

namespace IntegrationApasTotvs.Domain.Interfaces.Repositories
{
    public interface IPagamentoRepository : IRepository<Pagamento>
    {
        IEnumerable<Pagamento> GetByPedidoNoTracking(int pedidoId);
    }
}
