﻿using IntegrationApasTotvs.Domain.Entities;
using IntegrationApasTotvs.Domain.Interfaces.Repositories.Base;

namespace IntegrationApasTotvs.Domain.Interfaces.Repositories
{
    public interface IPrecoCongressoRepository : IRepository<PrecoCongresso>
    {
    }
}
