﻿using IntegrationApasTotvs.Domain.Entities;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace IntegrationApasTotvs.Domain.Interfaces.Services
{
    public interface IIntegrationService
    {
        Task Execute();
    }
}
